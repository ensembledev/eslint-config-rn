module.exports = {
  "parser": "babel-eslint",
  "extends": "@ensemblebr/eslint-config",
  "plugins": [
    "react",
    "react-native",
  ],
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true,
      "legacyDecorators": true
    }
  },
  "env": {
    "react-native/react-native": true
  },
  "rules": {
    "react-native/no-unused-styles": "error",
    "react-native/split-platform-components": "error",
    "react-native/no-inline-styles": "error",
    "react-native/no-color-literals": "error",
    "react-native/no-raw-text": "error",
  },
};
